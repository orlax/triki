var express = require('express'); 
var app = express(); 
var http = require('http'); 
var ws = require('ws'); 


app.set('view engine', 'pug'); 
app.use('/public', express.static(__dirname+'/public')); 

var port = '9090'; 

var sockets = {};
var rooms = {};
var uid = 0; 
var rid = 0; 

//room class to manage a game: 
function Room(){
    this.id = undefined;
    this.users = [];
    this.board = ['0','1','2','3','4','5','6','7','8']; //array con nueve elementos que representa la tabla de triki 
    this.freeSpaces = 9; 
    this.reset = function(){
        this.board = ['0','1','2','3','4','5','6','7','8']; 
        this.freeSpaces = 9; 
    }
}

var server = http.createServer(app).listen(port, function(){
    console.log("triki is running on http://localhost:"+port); 
});

var wss = new ws.Server({server:server}); 

wss.on('connection', function(ws){
    ws.on('message', function(msg){
        var data = JSON.parse(msg);
       // console.log("message received: "); 
       // console.log(data); 
        switch(data.type){
            case "register":
                RegisterNewUser(data.username, ws); 
            break; 
            case "newFigure": 
                DrawFigure(data, ws); 
            break; 
        }
    });
    
    // cuando el socket se cierra notificamos al otro usuario en su mismo room
    ws.on('close', function(reasonCode, description){
        if(ws.room && ws.id && !sockets[ws.id].isAlone){
            ws.room.users.forEach(user => {
                if(user.id != ws.id) {
                    sockets[user.id].send(JSON.stringify({type:"playerLeft"})); 
                    sockets[user.id].isAlone = true; 
                } 
            });
        }
        //si el usuario que se fue es el ultimo en haberse conectado agregamos 1 al index
        if(!(ws.id%2==0) && ws.id==uid) getNextId(); 
        //eliminamos el room 
        delete ws.room; 
    })
    
}); 

app.get('/', function(req,res){
    res.render('room');
});

function RegisterNewUser(username, ws){
    var response = {
        type:"registerResponse"
    }; 
    
    var id = getNextId(); 
    console.log("registering user: "+id); 
    sockets[id] = ws; 
    ws.id = id; 
    var newUser = {  name: username, id: id }; 
    // si es un numero par lo ponemos en el ultimo cuarto generado. 
    if(id%2==0){
        var room = rooms[rid]; 
        response.enemy = room.users[0].name; 
        room.users.push(newUser)
        response.room = room; 
        response.startGame = true; 
        response.playsWith = "X";
        ws.room = room; 
        //we send message of newPlayer to the other user
        newPlayer(room.users[0].id, username, "X"); 
        console.log("joined room: "+rid); 
    }
    // si es un numero impar debemos crear un nuevo cuarto para el. 
    else{
        var newRoom = new Room(); 
        newRoom.id = getNextRoomId(); 
        newRoom.users.push(newUser);
        rooms[newRoom.id]=newRoom; 
        ws.room = newRoom; 
        response.room = newRoom; 
        response.startGame = false; 
        response.playsWith = "O";
        console.log("created room: "+rid); 
    }

    response.username = username; 
    response.success = true; 

    ws.send(JSON.stringify(response)); 
}

function newPlayer(socketId, newPlayerName, playsWith){
    var msg = {
        type:"newPlayer", 
        username: newPlayerName, 
        playsWith: playsWith
    }; 
    sockets[socketId].send(JSON.stringify(msg)); 
}

function DrawFigure(data, ws){

    var room = rooms[ws.room.id]; 
    room.board[data.index] = data.playsWith; 
    room.freeSpaces--; 

    data.winner = CheckWinner(room.board); 

    if(data.winner != undefined){
        room.reset(); 
    }

    if(room.freeSpaces==0) {
            data.tie = true;
            room.reset(); 
    } 


    if(data.winner != undefined) ws.room.reset(); 

    ws.room.users.forEach(user => {
        sockets[user.id].send(JSON.stringify(data));
    });
}

function CheckWinner(b){
    console.log("winner check: ")
    console.log(b); 
    console.log( b[0] == b[1]);
    console.log( b[0] == b[1] == b[2] );
    console.log( b[0] == b[1] && b[1] == b[2] );
    var winner = undefined; 
    if( b[0] == b[1] && b[1] == b[2] ) winner = b[0];
    if( b[3] == b[4] && b[4] == b[5] ) winner = b[3];
    if( b[6] == b[7] && b[7] == b[8] ) winner = b[6];

    if( b[0] == b[3] && b[3] == b[6] ) winner = b[0];
    if( b[1] == b[4] && b[4] == b[7] ) winner = b[1];
    if( b[2] == b[5] && b[5] == b[8] ) winner = b[2];

    if( b[0] == b[4] && b[4] == b[8] ) winner = b[0];
    if( b[2] == b[4] && b[4] == b[6] ) winner = b[2];    

    console.log("winner check is: "+ winner); 
    return winner; 
}

function getNextId(){
    uid += 1; 
    console.log("next id is: " +uid); 
    return uid; 
}

function getNextRoomId(){
    rid += 1; 
    return rid; 
}